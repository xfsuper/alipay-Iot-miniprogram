# alipay-Iot-miniprogram
支付宝蜻蜓刷脸支付Iot小程序  
配套微信Iot小程序：[微信青蛙刷脸支付小程序](https://gitee.com/maidoududuzai/wxfacepay-Iot-miniprogram)  
配套Web后端系统：[服务商刷脸支付系统](https://gitee.com/maidoududuzai/pay-crm-osc)  

Iot小程序特殊API在模拟器中不可用，文档主要说明模拟器中的使用方法，真机调试参考[支付宝Iot小程序接入指南](https://opendocs.alipay.com/mini/006ksi)

# 硬件能力
- Usb小票打印机，型号佳博GP-58MBIII
- 魔线

# IDE设置
[支付宝小程序开发者工具](https://opendocs.alipay.com/mini/ide/download)
- 打开项目-项目类型-选择“支付宝Iot小程序”-打开 
- 详情
	- 项目配置-勾选“启用 component2 编译”
	- 域名信息-勾选“忽略 request 域名合法性检查”

# 使用方法
系统域名：demo.tryyun.net  
店员账号：12345678901 或 00000000000

进入设置页（密码123456）：  
- （模拟器）控制台输入 `getApp().setting.entry()`
- （真机）K1小键盘按“设置”键

首页退出登录：
- （模拟器）控制台输入 `getApp().setting.logout()`
- （真机）连续点击首页顶部胶囊按钮左侧隐藏按钮八次

# screenshot
![输入图片说明](https://images.gitee.com/uploads/images/2020/0527/110012_129f7d12_4857616.jpeg)

### 如有疑问，欢迎交流
QQ群：20533363
